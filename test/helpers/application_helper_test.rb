require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "POTEPAN CAMP Sub Task"
    assert_equal full_title("Help"), "Help | POTEPAN CAMP Sub Task"
  end
end