class AddToNoticeCheckedColumn < ActiveRecord::Migration[5.1]
  def change
    add_column :notices, :checked, :boolean, default: false, null: false
  end
end
