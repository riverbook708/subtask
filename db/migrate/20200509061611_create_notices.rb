class CreateNotices < ActiveRecord::Migration[5.1]
  def change
    create_table :notices do |t|
      t.integer :user_id
      t.integer :liker_id
      t.text :text
      t.timestamps
    end
  end
end
