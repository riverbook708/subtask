class AddColumnToUsersRealWebPrTelSex < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :website, :string
    add_column :users, :pr, :string
    add_column :users, :tel, :integer
    add_column :users, :sex, :string
  end
end
