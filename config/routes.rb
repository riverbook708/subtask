Rails.application.routes.draw do
  get 'notices/index'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  root 'static_pages#home'
  
  post '/likes/:micropost_id/destroy', to: 'likes#destroy'
  post '/likes/:micropost_id/create', to: 'likes#create' 
  get  '/help',    to:'static_pages#help'
  get  '/about',   to:'static_pages#about'
  get  '/contact', to:'static_pages#contact'
  get  '/signup',  to:'users#new'
  post '/signup',  to:'users#create'
  get  '/login',  to:'sessions#new'
  post '/login',  to:'sessions#create'
  delete '/logout',to:'sessions#destroy'
  resources :notices,           only: [:destroy]
  resources :users do
    member do
      get :following, :followers
    end
    resources :notices,           only: [:index]
  end
  get  '/users/:id/likes',   to:'users#likes'
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:index, :new, :show, :create, :destroy] do
    resources :comments,            only: [:create, :destroy]
  end
  
  resources :relationships,       only: [:create, :destroy]
end
