class ApplicationMailer < ActionMailer::Base
  default from: "riverbook708@gmail.com"
  layout 'mailer'
end