class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "Account activation"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "Password reset"
  end
  
  def added_to_favorites(user, liker)
    @user = user
    @liker = liker
    mail to: user.email, subject: "Added to Favorites"
  end
  
  def comments_to_micropost(user, liker)
    @user = user
    @liker = liker
    mail to: user.email, subject: "Commented on your micropost"
  end
end