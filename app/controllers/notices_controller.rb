class NoticesController < ApplicationController
  def index
    @user = User.find_by(id: params[:user_id])
    @notices = Notice.where(user_id: params[:user_id]).order(created_at: "DESC")
    @notices.where(checked: false).each do |notice|
      notice.update_attributes(checked: true)
    end
  end
  
  def destroy
    @notices = Notice.where(user_id: current_user.id)
    @notices.each do |notice|
      notice.destroy
    end
    redirect_to "/users/#{current_user.id}/notices"
  end
end
