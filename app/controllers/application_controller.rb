class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  def unchecked_notices
    @notices = Notice.find_by(user_id: current_user.id ,checked: false)
  end
  

  
  private
  
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
end
