class UsersController < ApplicationController
  before_action :logged_in_user, only:[:index,:edit,:update, :destroy, :following, :followers]
  before_action :correct_user, only:[:edit,:update]
  before_action :admin_user,     only: :destroy

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to root_url
  end
  
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def likes
    @user = User.find(params[:id])
    @likes = @user.likes.paginate(page: params[:page])
  end

  
  private
  
    def user_params
      params.require(:user).permit(:real_name, :name, :website, :pr,
                                    :email, :tel, :sex, :password,
                                    :password_confirmation)
    end
    
    
    
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    def admin_user
      @user =User.find(params[:id])
      if current_user.admin? || (current_user.id == @user.id)
      else
        redirect_to(root_url) 
      end
    end
  
end
