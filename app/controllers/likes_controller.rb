class LikesController < ApplicationController
  before_action :logged_in_user

  def create
    @like = Like.new(user_id: current_user.id, micropost_id: params[:micropost_id])
    @like.save
    @post = Micropost.find_by(id: params[:micropost_id])
    @user = User.find_by(id: @post.user_id)
   @liker = current_user
    Notice.create(text: "Your image has been added to #{@liker.name}'s favorites",
                  user_id: @user.id,
                  liker_id: @liker.id)
    UserMailer.added_to_favorites(@user, @liker).deliver_now
    redirect_back(fallback_location: root_path)
  end
  
  def destroy
    @like = Like.find_by(user_id: current_user.id, micropost_id: params[:micropost_id])
    @like.destroy
   redirect_back(fallback_location: root_path)
  end
end