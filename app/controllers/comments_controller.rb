class CommentsController < ApplicationController
  def create
    @comment = Comment.create(text:         params[:text], 
                   micropost_id: params[:micropost_id], 
                   user_id:      current_user.id)
    @post = Micropost.find_by(id: params[:micropost_id])
    @user = User.find_by(id: @post.user_id)
    @liker = current_user
    Notice.create(text: "#{@liker.name} commented on your post",
                  user_id: @user.id,
                  liker_id: @liker.id)
    UserMailer.comments_to_micropost(@user, @liker).deliver_now
    redirect_to "/microposts/#{@comment.micropost.id}"
  end
  
  def destroy
  end
  
  private
    def comment_params
      params.permit(:text, :micropost_id)
    end
end
