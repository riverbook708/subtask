module NoticesHelper
  def unchecked_notices
    @notices = Notice.find_by(user_id: current_user.id ,checked: false)
  end
end
