class Comment < ApplicationRecord
    validates :text, presence: true
    belongs_to :micropost
    belongs_to :user
end
